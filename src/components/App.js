import React, { Component } from 'react'
import  color from './color'
export default class App extends Component {
    render() {
        return (
            <div style={{color:color}}>
                <h1>My React app</h1>
            </div>
        )
    }
}
